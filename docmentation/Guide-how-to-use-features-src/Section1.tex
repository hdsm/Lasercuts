\section{Introduction}

This guide explains how to use the ``features'' on a closed box.
It will start from the model ``kaesseli'' and transform it to the model
``rolling-die'', explaining along the way what features are and introducing
ways to use them.\
\footnote{``Kaesseli'' is a German diminutive of ``kassa'', i.e. a piggy
 bank.}

\todo[inline]{lstlistings must be ttfamily}
\todo[inline]{first show picture of original kaesseli before you show modified version}
\todo[inline]{Pictures should be in figures and have a nice caption.}
\todo[inline]{explain parameter "shape"}
\todo[inline]{Write appendix, all full codes}

\section{Rolling Dice for Patrick}

\paragraph{Patrick's Situation}
Patrick has already created the model ``kaesseli'', cut the plans with a
LASER, and has started to put money into this piggy bank. He doesn't like
that the coin slit in ``kaesseli'' is very small by default
so Patrick can only save little amounts of money, but he has more pressing
needs: the rolling dice of his mononpoly board are lost! Swiftly
he decides that a LASER cut solution must do.\footnote{After all, a rolling
die and a piggy bank differ only in some small parameters. It would be more
difficult to see similarities between, say, a bird house and a rolling die.}

\paragraph{Basic setup of the box -- there are six individual plates}
First Patrick looks inside the file {\ttfamily kaesseli.yaml} and notices
that there are six plates mentioned:

\begin{lstlisting}

featuresFrontplate: []

featuresBackplate: []

featuresRightplate: []

featuresLeftplate: []

featuresBottomplate: []

featuresTopplate:
...
\end{lstlisting}

It is even clear which plate means what, e.g.\ {\ttfamily featuresFrontplate}
is for the front plate, {\ttfamily featuresBottomplate} is for the bottom plate, etc.

All plates are the same, except the top plate.\footnote{A well known property
of piggy banks.}

\paragraph{Overview of one plate -- the first encounter with features}
Patrick decides to have a closer look at the description
of the top plate:
\begin{lstlisting}
featuresBottomplate: []

featuresTopplate:
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '18mm'
          - parameter: '2mm'
\end{lstlisting}

Here we see the gist of what the ``features'' are and how to use them.

\begin{itemize}
\item All decarations of plates follow the pattern
	\begin{lstlisting}
	nameOfThePlate: <listOfFeatures>
	\end{lstlisting}
	(note the colon which defines when the list of features start).

\item A featureless plate needs to be declared with a feature of an empty
	list, denoted with square brackets.
	\begin{lstlisting}
	featuresBottomplate: []
	\end{lstlisting}
\item A plate with a feature needs to define the following things:
	\begin{itemize}
	\item which feature to use,
	\begin{lstlisting}
	feature: 'rectangle'
	\end{lstlisting}

	\item the position of the feature
	\begin{lstlisting}
	xshift: '17mm'
	yshift: '35mm'
	\end{lstlisting}
	\item the parameters of the feature (typically each feaure
		has its own specific set of parameters).
	\end{itemize}
\end{itemize}

\paragraph{A closer look at a feature and how to adapt it}
Cleverly Patrick decides to use the parameters of the feature and ``reshape''
it, such that it looks like a square; that way Patrick has a rolling die
with at least a pretty 1-face. Fiddling around\footnote{The hashes ``\#'' are
introduced by Patrick as comments. That means everything from \# will be ignored until
the end of the line. This is how one can make useful notes, you will find them
anywhere as little helpers.}
he finds these to be the best
values:

\begin{lstlisting}
featuresBottomplate: []

featuresTopplate:
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
\end{lstlisting}

giving the following plans:

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-10.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

Super! Patrick already has something which is definitively not a piggy bank,
and he starts throwing it around like a die. Soon he realises that his
1-face (i.e.\ the plate with the square hole) does not show up in
$\nicefrac{1}{6}$ of his throws because of the shape of the box.
This can be fixed with making the box more regular.

\paragraph{Parameters concerning the overall shape of the box -- setting
the size of the cuboid}
For this Patrick goes back to the first few parameters in
the file {\ttfamily kaesseli.yaml} and notices these important parameters:

\begin{lstlisting}
boxHeight: 6
boxWidth: 12
boxLength: 16
fingerWidth: '2mm'
\end{lstlisting}

These define the overall shape of the box, in this case a cuboid of ratio
$6:12:16$. Changing these will give a different cuboid, e.g. a ratio
of $6:12:12$, will yield an already more regular cuboid:

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-20.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

And further changing the ratio to $12:12:12$ gives

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-30.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

This looks already like a plan for a regular cube and should yield the 1-face
in about $\nicefrac{1}{6}$ of the time. Now Patrick only has to create the
other faces.

\paragraph{Changing positions of featues}
For this Patrick has an idea: he includes the \emph{feature}
of a square hole onto each plate:

\begin{itemize}
\item onto the 2-face he includes the feature two times, each at a different position
\item onto the 3-face he includes the feature three times, each at the according positon
\item etc. etc.
\end{itemize}

Including the feature onto another plate is easy, just add the same
feature to another plate (\emph{and make sure to delete the square
brackets when adding some feature!}\footnote{This is because, for technical
reasons, each plate needs some feature, and this can also be the ``empty
feature'' which is denoted by the square brackets {\ttfamily []}}):

\begin{lstlisting}
featuresFrontplate: []

featuresBackplate: []

featuresRightplate: 
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

featuresLeftplate: []

featuresBottomplate: []

featuresTopplate:
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
\end{lstlisting}

Yay, we actually have another hole:

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-40.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

But adding the same feature \emph{twice}, i.e. doing something like
\begin{lstlisting}
featuresRightplate: 
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
\end{lstlisting}
will give the same(-looking result) like before. This is because Patrick has
include the same feature \emph{at the exact same position}! To change this,
let's have a closer look at one feature:
\begin{lstlisting} 
    - xshift: '17mm'
      yshift: '35mm'
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
\end{lstlisting}

\subparagraph{The parameters responsible to the position of the feature}
We see that the feature itself is accompanied by parameters (denoted {\ttfamily xshift} and
{\ttfamily yshift}, respectively). These parameters are not parameters
inherent in \emph{this} feature, but are available to \emph{every} feature
which is included.
That is, every feature has the paremeters {\ttfamily xshift},
{\ttfamily yshift} (and the feature may or may not come with a set of further
parameter(s), like, size, radius, color, etc., which we have already seen
we adapted the coin-slit to a square.).

So, changing these parameters to e.g.
\begin{lstlisting}
featuresRightplate: 
    - xshift: '17mm' # x position
      yshift: '35mm' # y position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '35mm' # x position
      yshift: '17mm' # y position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

\end{lstlisting}

will give the following plans:


\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-50.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

\paragraph{Finishing the rolling die by putting features into place}
Patrick has now all the tools and knowledge ready to finish a rolling die.
In particular he can shift the position of the 1-face to put it in the
center, and also tweak the positions of the 2-face's features to make it
better looking.

\subparagraph{The 2-face}
These entries in {\ttfamily kaesseli.yaml}

\begin{lstlisting}
# 2-face
featuresRightplate:
    - xshift: '12mm' # x-position
      yshift: '32mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '30mm' # x-position
      yshift: '11mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

# 1-face
featuresTopplate:
    - xshift: '23mm' # x-position
      yshift: '23mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

\end{lstlisting}

will give these plans:

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-60.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

\subparagraph{The 3-face}
Inputting this code excerpt (i.e.\ changing the features of yet another plate)

\begin{lstlisting}
# 3-face
featuresBackplate:
    - xshift: '9mm' # x-position
      yshift: '35mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '21mm' # x-position
      yshift: '21.5mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '33mm' # x-position
      yshift: '8mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

\end{lstlisting}

will give these plans:

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-70.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

\subparagraph{Finishing up the rolling die}
Patrick loves these features so much that he proceeds to finish all six faces
with his newly gained experience.

The following plans

\begin{minipage}{0.9\textwidth}
\includegraphics[scale=0.3]{\sourcedir/graphics/Patrick-80.pdf}
(Note that this image is not the actual size of the plan).
\end{minipage}

have been created with this complete list of plates and features:
\begin{lstlisting}
# 6-face
featuresFrontplate:
    # top left
    - xshift: '10mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom right
    - xshift: '32mm' # x-position
      yshift: '9mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # top right
    - xshift: '32mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom left
    - xshift: '10mm' # x-position
      yshift: '9mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom center
    - xshift: '21mm' # x-position
      yshift: '9mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # top center
    - xshift: '21mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

# 3-face
featuresBackplate:
    - xshift: '9mm' # x-position
      yshift: '35mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '21mm' # x-position
      yshift: '21.5mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '33mm' # x-position
      yshift: '8mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

# 2-face
featuresRightplate:
    - xshift: '12mm' # x-position
      yshift: '32mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    - xshift: '30mm' # x-position
      yshift: '11mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

# 5-face
featuresLeftplate:
    # top left
    - xshift: '10mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom right
    - xshift: '32mm' # x-position
      yshift: '9mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # top right
    - xshift: '32mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom left
    - xshift: '10mm' # x-position
      yshift: '9mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # center
    - xshift: '21mm' # x-position
      yshift: '21.5mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

# 4-face
featuresBottomplate:
    # top left
    - xshift: '14mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom right
    - xshift: '32mm' # x-position
      yshift: '13mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # top right
    - xshift: '32mm' # x-position
      yshift: '34mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y
    # bottom left
    - xshift: '14mm' # x-position
      yshift: '13mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

# 1-face
featuresTopplate:
    - xshift: '23mm' # x-position
      yshift: '23mm' # y-position
      feature: 'rectangle'
      parameters:
          - parameter: '6.4mm' # size x
          - parameter: '6.4mm' # size y

\end{lstlisting}
