= Public Domain Lasercuts by HDSM =

== Introduction and Overview ==

* What is this?
  - This is a project to have Lasercut designs generated automatically.
    Usually, making something with a LASER consists of the following steps:

    1. Think of what you want.
    2. Design the plans of the thing you want. (This takes much time).
    3. Go somewhere with a LASER cutter to cut them out.

    Our goal is to have the following:

    1. Think of what you want (currently box-shaped things)
    2. Input the parameters into the Software (height, width, features, etc.)
    3. Click a button and go to a LASER cutter.

* What is the state of the project?
  - This project is work-in-progress, so there is a lot of stuff which can go
    wrong.


== Preparations ==

* Currently, the following dependencies have to be met.
  * You have a working LaTeX installation
  * You know how to "run a make file". In the simplest case this consists of
    - opening a terminal
    - changing to the folder of the model you want to make
    - typing `make MyBox' where MyBox is the model you want to make.


== Making a box ==

So, you have decided that you actually want to make a box with this software.
This is how you do it.

1. Take the yaml file of the template you want to make. Edit this yaml file
    according to you needs (width, length, height, etc.)
2. Run

     # make MyMakeTargetName

   where MyMakeTargetName is the make target (usually the same name of the yaml
   file, except if you change stuff).


