#!/usr/bin/env python3

import sys
import yaml

# Get file name from command line
if(len(sys.argv) != 3):
    print('usage: ', sys.argv[0], ' ', 'yaml_file template_file')
    sys.exit(1)

yaml_file     = sys.argv[1];
template_file = sys.argv[2];

# Load data
f_data = open(yaml_file, 'r')
data = yaml.load(f_data)
f_data.close()

# Process template
import template_processor
tpl = open(template_file).read()

# Print result
print(template_processor.process(tpl, data))
