<{title}>
===

Numbers
---

<[number_dictionaries]>
### English to <{language}>

<[dictionary]>
one

: <{one}>

two

: <{two}>
<[/dictionary]>
<[/number_dictionaries]>

Colors
---

<[color_dictionaries]>
### English to <{language}>

<[dictionary]>
<{color}>

: <{translation}>

<[/dictionary]>

<[/color_dictionaries]>
