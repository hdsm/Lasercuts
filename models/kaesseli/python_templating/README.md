Super advanced templating
=========================

This is a minimal templating python script using `re.sub` from the `re`
module[1].

Examples
--------

Single patterns of the form `<{identifier}>`:

	>>> import template_processor as tp
	>>> tp.process( \
	...     'Name: <{name}>, Date: <{date}>', \
	...     {'name': 'David', 'date': '2016-07-03'})
	'Name: David, Date: 2016-07-03'

Nested patterns of the from: `<[identifier]> content <[/identifier]>`:

	>>> template = \
	... '''# Contacts
	... <[contacts]>
	... ## <{name}>
	... <[phones]>
	...   - <{type}>: <{number}>
	... <[/phones]>
	... <[/contacts]>'''
	>>> replaces = \
	...     {'contacts': \
	...         [{'name':'Davide', \
	...           'phones': [ \
	...              {'type':'fix','number':'482347'}, \
	...              {'type':'mobile','number':'3824937'}]}, \
	...          {'name':'Paolo', \
	...           'phones': [ \
	...              {'type':'mobile','number':'837981'}]}, \
	...          {'name':'Gianni', \
	...           'phones': [ \
	...              {'type':'fix','number':'329487239'}]}]}
	>>> print(tp.process(template, replaces))
	# Contacts
	## Davide
	  - fix: 482347
	  - mobile: 3824937
	## Paolo
	  - mobile: 837981
	## Gianni
	  - fix: 329487239


Test this examples with `python -m doctest -v README.md`

Links
-----

[1]: https://docs.python.org/3.2/howto/regex.html
