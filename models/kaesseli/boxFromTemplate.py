#!/usr/bin/python

import argparse
import yaml
import sys

import python_templating.template_processor as tp

# argument parser
def dimensionsType(s):
    try:
        x, y, z = map(int, s.split(','))
        return x, y, z
    except:
        raise argparse.ArgumentTypeError('dimensions must be height, width, depth')

parser = argparse.ArgumentParser(description='Generate box from template')
parser.add_argument('-d', '--dimensions',
                   dest='box_dimensions',
                   type=dimensionsType,
                   help='Box dimensions: height, width, depth')
parser.add_argument('-t', '--template',
                   dest='template_file',
                   type=str,
                   help='template file')
parser.add_argument('-y', '--yaml',
                   dest='yaml_file',
                   type=str,
                   help='yaml file')
args = parser.parse_args()


# replacements dictionary
box_data = {}

# load yaml file if specified
if args.yaml_file:
    f_data = open(args.yaml_file, 'r')
    box_data = yaml.load(f_data)
    f_data.close()
else:
    sys.exit('No yaml specified')

# load box dimensions given as argument
if args.box_dimensions:
    box_data['boxHeight'] = args.box_dimensions[0]
    box_data['boxWidth'] = args.box_dimensions[1]
    box_data['boxDepth'] = args.box_dimensions[2]

if args.template_file:
    template_file = args.template_file
elif 'templateFile' in box_data:
    template_file = box_data['templateFile']
else:
    sys.exit('No template file specified')

box_tpl = open(template_file).read()

box_src = tp.process(box_tpl, box_data)

print(box_src)
